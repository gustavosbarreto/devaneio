package gateway

import (
	"html/template"
	"net/http"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

type Admin struct {
	Gateway *Gateway
	Router *echo.Echo
}

func NewAdmin(g *Gateway) *Admin {
	a := &Admin{
		Gateway: g,
		Router: echo.New(),
	}

	a.Router.Renderer = &Template{
		templates: template.Must(template.ParseGlob("ui/*.html")),
	}

	return a
}

func (a *Admin) Setup() {
	a.Router.Pre(middleware.AddTrailingSlash())

	r := a.Router.Group("/admin")
	r.GET("/", func(c echo.Context) error {

		return c.Render(http.StatusOK, "index.html", map[string]interface{}{
			"Routes": a.Gateway.Routes,
		})
	})
}
