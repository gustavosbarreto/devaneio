package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

var clean bool

func apiRouter(w http.ResponseWriter, r *http.Request) {
	fmt.Println("aqui")

	fmt.Println(r.URL.Path)
}

func main() {
	rr := mux.NewRouter()

	clean = false

	rr.HandleFunc("/updateroutes", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("DEU"))

		clean = !clean

	})

	rr.PathPrefix("/api").HandlerFunc(apiRouter)
	//	rr.PathPrefix("/api").Handler(http.Handler(apiRouter()))

	http.ListenAndServe(":8080", rr)
}
