package main

import (
	"fmt"
	"net/http"

	"bitbucket.org/gustavosbarreto/devaneio/gateway"
	"github.com/gorilla/mux"
	"github.com/labstack/echo"
	mgo "gopkg.in/mgo.v2"
)

type Application struct {
	Listen string `yaml:"listen,omitempty"`
	API    *echo.Group
}

func main() {
	mongo, err := mgo.Dial("172.17.0.2")
	if err != nil {
		panic(err)
	}

	mongo.SetSafe(&mgo.Safe{
		W: 1,
		J: true,
	})

	g := gateway.NewGateway()
	g.Mongo = mongo

	err = g.UpdateRoutes()
	if err != nil {
		panic(err)
	}

	admin := gateway.NewAdmin(g)
	admin.Setup()

	server := mux.NewRouter()
	server.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<h1>devaneio mental</h1>")
	})
	server.PathPrefix("/api").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		g.Router.ServeHTTP(w, r)
	})
	server.PathPrefix("/admin").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		admin.Router.ServeHTTP(w, r)
	})

	if err := http.ListenAndServe(":8080", server); err != nil {
		panic(err)
	}
}
