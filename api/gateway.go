package gateway

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
	"strconv"
	"strings"

	"github.com/labstack/echo"
	mgo "gopkg.in/mgo.v2"
)

type Gateway struct {
	Router *echo.Echo
	Routes []Route
	Mongo  *mgo.Session
}

func NewGateway() *Gateway {
	return &Gateway{}
}

func (g *Gateway) UpdateRoutes() error {
	sess := g.Mongo.Copy()
	defer sess.Close()

	err := sess.DB("gateway").C("routes").Find(nil).All(&g.Routes)
	if err != nil {
		return err
	}

	r := echo.New()
	api := r.Group("/api")

	for _, route := range g.Routes {
		switch route.Method {
		case "GET":
			api.GET(route.Path, Handler(route))
		case "POST":
			api.POST(route.Path, Handler(route))
		case "PUT":
			api.PUT(route.Path, Handler(route))
		case "DELETE":
			api.DELETE(route.Path, Handler(route))
		}

		if route.Static {
			api.Static(route.Path, route.Root)
			continue
		}
	}

	g.Router = r

	return nil
}

func Handler(route Route) func(echo.Context) error {
	return func(c echo.Context) error {
		script, err := ioutil.TempFile("", "script")
		if err != nil {
			log.Fatal(err)
		}

		defer os.Remove(script.Name())

		if _, err := script.Write([]byte(route.Script)); err != nil {
			log.Fatal(err)
		}

		if err := script.Close(); err != nil {
			log.Fatal(err)
		}

		args := []string{}
		args = append(args, script.Name())

		cmd := exec.Command(route.Engine, args...)
		cmd.Env = []string{}

		for _, name := range c.ParamNames() {
			cmd.Env = append(cmd.Env, fmt.Sprintf("PARAM_%s=%s", strings.ToUpper(name), c.Param(name)))
		}

		for name, values := range c.QueryParams() {
			cmd.Env = append(cmd.Env, fmt.Sprintf("QUERY_%s=%s", strings.ToUpper(name), values[0]))
		}

		for key, value := range c.Request().Header {
			cmd.Env = append(cmd.Env, fmt.Sprintf("HEADER_%s=%s", strings.ToUpper(key), value))
		}

		if route.Upload != "" {
			upload, err := c.FormFile(route.Upload)
			if err != nil {
				return c.NoContent(http.StatusInternalServerError)
			}

			src, err := upload.Open()
			if err != nil {
				return c.NoContent(http.StatusInternalServerError)
			}
			defer src.Close()

			if err = os.MkdirAll("uploads", 0700); err != nil {
				return c.NoContent(http.StatusInternalServerError)
			}

			dst, err := os.Create(path.Join("uploads", upload.Filename))
			if err != nil {
				fmt.Println(err)
				return c.NoContent(http.StatusInternalServerError)
			}
			defer dst.Close()

			defer func() {
				os.Remove(dst.Name())
			}()

			if _, err = io.Copy(dst, src); err != nil {
				return c.NoContent(http.StatusInternalServerError)
			}

			cmd.Env = append(cmd.Env, fmt.Sprintf("UPLOAD_%s=%s", strings.ToUpper(route.Upload), upload.Filename))
		}

		stdin, err := cmd.StdinPipe()
		if err != nil {
			return err
		}

		defer stdin.Close()

		stdout, err := cmd.StdoutPipe()
		if err != nil {
			return err
		}

		defer stdout.Close()

		hdir, err := ioutil.TempDir("", "headers")
		if err != nil {
			return c.NoContent(http.StatusInternalServerError)
		}

		defer os.RemoveAll(hdir)

		cmd.Env = append(cmd.Env, fmt.Sprintf("HEADERS=%s", hdir))

		err = cmd.Start()
		if err != nil {
			return c.NoContent(http.StatusInternalServerError)
		}

		payload, err := ioutil.ReadAll(c.Request().Body)
		if err != nil {
			return c.NoContent(http.StatusInternalServerError)
		}

		stdin.Write(payload)
		stdin.Close()

		output, err := ioutil.ReadAll(stdout)
		if err != nil {
			return c.NoContent(http.StatusInternalServerError)
		}

		if err = cmd.Wait(); err != nil {
			httpCode, err := strconv.Atoi(strings.TrimSpace(string(output)))
			if err == nil {
				return c.NoContent(httpCode)
			}

			return c.NoContent(http.StatusInternalServerError)
		}

		headers, err := ioutil.ReadDir(hdir)
		if err != nil {
			return c.NoContent(http.StatusInternalServerError)
		}

		for _, f := range headers {
			header, _ := ioutil.ReadFile(path.Join(hdir, f.Name()))
			c.Response().Header().Set(f.Name(), string(header))
		}

		return c.String(http.StatusOK, string(output))
	}
}
