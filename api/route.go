package gateway

import "gopkg.in/mgo.v2/bson"

type Route struct {
	ID     bson.ObjectId `bson:"_id,omitempty"`
	Host   string        `bson:"host,omitempty"`
	Path   string        `bson:"path,omitempty"`
	Method string        `bson:"method,omitempty"`
	Upload string        `bson:"upload,omitempty"`
	Static bool          `bson:"static,omitempty"`
	Root   string        `bson:"root,omitempty"`
	Script string        `bson:"script,omitempty"`
	Engine string        `bson:"engine,omitempty"`
}
