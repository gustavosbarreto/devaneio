import React from 'react';
import { jsonServerRestClient, Admin, Resource } from 'admin-on-rest';

import { PostList } from './posts';

const App = () => (
    <Admin restClient={jsonServerRestClient('http://admin:8080')}>
        <Resource name="routes" list={RouteList} />
    </Admin>
);

export default App;
